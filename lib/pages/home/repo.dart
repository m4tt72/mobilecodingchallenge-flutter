import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class Repo extends StatelessWidget {
  final String name;
  final String description;
  final String owner;
  final String stars;

  Repo({
    @required this.name,
    @required this.description,
    @required this.owner,
    @required this.stars
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            name,
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 10),
          Text(
            description,
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.normal,
            )
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.image),
                  SizedBox(width: 5),
                  Text(
                    owner,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.star),
                  SizedBox(width: 5),
                  Text(stars)
                ],
              )
            ]
          )
        ],
      ),
    );
  }
}