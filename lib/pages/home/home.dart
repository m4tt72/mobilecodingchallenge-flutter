import 'package:flutter/material.dart';

import './repo.dart';
import './../../providers/github.dart';
import './../../models/result.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScrollController _controller = ScrollController();
  Result _repos;
  int _page = 1;
  bool _loading = true;

  @override
  void initState() {
    super.initState();
    _getRepos();
    _controller.addListener(() {
      if (_controller.position.maxScrollExtent - _controller.position.pixels <=
          350.0) {
        _getMoreRepos();
      }
    });
  }

  _getRepos() async {
    try {
      var result = await GithubProvider.getRepositories(_page);
      setState(() {
        _repos = result;
        _loading = false;
      });
    } catch (error) {
      print(error);
    }
  }

  Future<void> _getMoreRepos() async {
    if (!mounted) return;
    try {
      var result = await GithubProvider.getRepositories(++_page);
      setState(() {
        _repos.items.addAll(result.items);
      });
    } catch (error) {
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Trending Repos"),
          elevation: 0,
        ),
        body: _loading ? Center(
          child: CircularProgressIndicator(),
        ) : ListView(
          controller: _controller,
          children: _repos.items
              .map((item) => Repo(
                    name: item.name ?? "",
                    description: item.description ?? "",
                    owner: item.owner.login ?? "",
                    stars: item.stargazersCount.toString() ?? "",
                  ))
              .toList(),
        )
      );
  }
}
