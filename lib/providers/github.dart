import 'dart:convert';
import 'package:http/http.dart' as Http;

import '../models/result.dart';

class GithubProvider {
  static final String _baseUrl =
      "https://api.github.com/search/repositories?q=created:%3E2017-10-22&sort=stars&order=desc";

  static Future<Result> getRepositories(int page) async {
    try {
      var response = await Http.get("$_baseUrl&page=$page");
      if (response.statusCode == 200) {
        return Result.fromJson(json.decode(response.body));
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}
