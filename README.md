# mobilecodingchallenge

Mobile coding challenge

## Instruction

Run application

`flutter run`

Build application

`flutter build apk`

**notes**

Make sure you have [flutter](https://flutter.dev/docs/get-started/install) installed on your machine and [Android SDK](https://developer.android.com/studio) setup correctly